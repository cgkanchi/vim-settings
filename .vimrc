colorscheme desert
let mapleader=","
set autoindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set number
set smarttab
filetype on
filetype plugin on
filetype plugin indent on
set ruler
syntax on
set ignorecase
set smartcase
set backspace=2
set backspace=indent,eol,start
"let Tlist_Ctags_Cmd='C:/Users/Chinmay/vimfiles/plugin/ctags.exe'
set nocompatible
set scrolloff=3
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest,full
set ruler
"set undofile
"nnoremap / /\v
"vnoremap / /\v
set gdefault
set incsearch
"set showmatch
"set hlsearch
"nnoremap <leader><space> :noh<cr>
"nnoremap <tab> %
"vnoremap <tab> %
"au FocusLost * :wa
